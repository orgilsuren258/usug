import {
  Box,
  Typography,
  FormGroup,
  FormControlLabel,
  Button,
  Stack,
  Divider,
} from "@mui/material";
import Link from "next/link";
import { loginType } from "../../../src/types/auth/auth";
import CustomCheckbox from "../../../src/components/forms/theme-elements/CustomCheckbox";
import CustomTextField from "../../../src/components/forms/theme-elements/CustomTextField";
import CustomFormLabel from "../../../src/components/forms/theme-elements/CustomFormLabel";

import AuthSocialButtons from "./AuthSocialButtons";
import { doLogin } from "store/apps/userProfile/UserProfileSlice";
import { useState } from "react";
import { useDispatch } from "store/Store";
import { Login } from "api_service/user";
import { useRouter } from "next/router";

const AuthLogin = ({ title, subtitle, subtext }: loginType) => {

  let [phone, setPhone] = useState<any>(null);
  let [password, setPassword] = useState<any>(null);
  let dispatch = useDispatch();
  let router = useRouter();

  const onLoginSubmit = async (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    let param = {
      "phone": phone,
      "password": password
    }
    let result = await Login(param);

    
    if(result.data.RetData){
      dispatch(doLogin({
        status: true
      }))
      // router.push('/');
    }
  }

  return(
    <>
      {title ? (
        <Typography fontWeight="700" variant="h3" mb={1}>
          {title}
        </Typography>
      ) : null}

      {subtext}

      <AuthSocialButtons title="Sign in with" />
      <form onSubmit={onLoginSubmit}>
      <Box mt={3}>
        <Divider>
          <Typography
            component="span"
            color="textSecondary"
            variant="h6"
            fontWeight="400"
            position="relative"
            px={2}
          >
            or sign in with
          </Typography>
        </Divider>
      </Box>

      <Stack>
        <Box>
          <CustomFormLabel htmlFor="username">Username</CustomFormLabel>
          <CustomTextField 
            onChange={(e: any) => {
              setPhone(e.target.value);
            }}
            id="username" 
            variant="outlined" 
            fullWidth 
          />
        </Box>
        <Box>
          <CustomFormLabel htmlFor="password">Password</CustomFormLabel>
          <CustomTextField
            onChange={(e: any) => {
              setPassword(e.target.value);
            }}
            id="password"
            type="password"
            variant="outlined"
            fullWidth
          />
        </Box>
        <Stack
          justifyContent="space-between"
          direction="row"
          alignItems="center"
          my={2}
        >
          <FormGroup>
            <FormControlLabel
              control={<CustomCheckbox defaultChecked />}
              label="Remeber this Device"
            />
          </FormGroup>
          <Typography
            component={Link}
            href="/auth/forgot-password"
            fontWeight="500"
            sx={{
              textDecoration: "none",
              color: "primary.main",
            }}
          >
            Forgot Password ?
          </Typography>
        </Stack>
      </Stack>
      <Box>
        <Button
          color="primary"
          variant="contained"
          size="large"
          fullWidth
          component={Link}
          href="/"
          onClick={onLoginSubmit}
          type="button"
        >
          Sign In
        </Button>
      </Box>
      </form>
      {subtitle}
    </>
  )
};

export default AuthLogin;
