import { Box, Button } from '@mui/material';
import Breadcrumb from 'layouts/full/shared/breadcrumb/Breadcrumb';
import PageContainer from 'components/container/PageContainer';
import ProductTableList from 'components/apps/ecommerce/ProductTableList/ProductTableList';
import Link from 'next/link';

const BCrumb = [
  {
    to: '/',
    title: 'Home',
  },
  {
    title: 'Search Table',
  },
];

export default function SearchTable() {

  return (
    <PageContainer>
      {/* breadcrumb */}
      <Breadcrumb title="Search Table" items={BCrumb} />
      {/* end breadcrumb */}
      <Box>
        <Button component={Link} href={`/user/new`} variant="contained" style={{marginLeft: "15px"}}>New User</Button>
        <ProductTableList />
      </Box>
    </PageContainer>
  );
};

