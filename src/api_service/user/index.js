import axios from "utils/axios";
import axios_no_auth from "utils/axios_no_auth";

export async function Login(param){
  const response = await axios_no_auth.post(`/usug01/LoginData`, param);
  if(response.data.RetData){
    localStorage.setItem('token', response.data.RetData.access_token)
  }
  return response;
}

export async function CreateUser(params){
  if(params.userid >= 0){
    params.optype = 1;
  }else{
    params.userid = -1;
    params.optype = 0;
  }
  console.log(params);
  const response = await axios.post(`/usug01/register`, params);
  console.log(response);
  return response;
}

export async function GetUsers(){
  const response = await axios.post(`/usug01/getprotypes`, {
    optype: 3
  });
  return response.data.RetData;
}

export async function CheckAuth(token){
  const response = await axios.post(`/usug01/chktoken`, {});
  console.log("RESPONSE", response);
  return response.data.RetData;
}

export async function DeleteUser(params){
  params.optype = 2;
  console.log(params);
  const response = await axios.post(`/usug01/register`, params);
  console.log(response);
  return response; 
}

export async function GetUser(param){
  const response = await axios.post(`/usug01/userinfo`, param.queryKey[1]);
  return response.data.RetData;
}

// export {Login, CreateUser, GetUsers, CheckAuth};