import axios from "utils/axios";

async function getProTypes(param){
  const response = await axios.post(`/usug01/getprotypes`, param.queryKey[1]);
  return response.data.RetData;
}

export {getProTypes};