import { Box } from "@mui/material";
import { redirect } from "next/navigation";
import { useRouter } from "next/router";
import { AppState, useSelector } from "store/Store";

interface BlankLayoutProps {
  children: React.ReactNode;
}

const BlankLayout = ({ children }: BlankLayoutProps) => {

  let router = useRouter();
  let user = useSelector((state: AppState) => state.userpostsReducer.user);

  if(user.is_logged){
    router.push('/');
  }

  return (
    <Box>{children}</Box>
  )
};

export default BlankLayout;



