import axios from 'axios';
import env_variables from '../../config/const.json'

// axios.defaults.withCredentials = true;

const axiosServices = axios.create({
    // withCredentials: true,
baseURL: env_variables.API_URI,
headers: {
    'Content-Type': 'application/json',
},
timeout: 2000,
});

axiosServices.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization =  `Bearer ${token}`;
    return config;
});

axiosServices.interceptors.response.use(
    (response) => {
        console.log(response);
        return response
    },
    (error) => {
        // console.log(error);
        if(error.response) {
            // console.log(error.response);

            if(error.response.status >= 401){
                localStorage.setItem('token', '');
                window.location = '/auth/login';
            }

            if((error.response.status >= 400 && error.response.status <= 499) || error.response.status === 500){
                window.location = '/404';
            }
            if(error.response.status >= 503) {
                window.location.reload();
            }
        }
        return Promise.reject(error);
    }
);

export default axiosServices;
 