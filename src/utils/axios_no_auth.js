import axios_no_auth from 'axios';
import env_variables from '../../config/const.json'

console.log(env_variables);

const axiosServicesNoAuth = axios_no_auth.create({
    baseURL: env_variables.API_URI
});

export default axiosServicesNoAuth;
 