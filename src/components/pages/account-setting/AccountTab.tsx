import React, { useEffect, useMemo, useState } from "react";
import {
  CardContent,
  Grid,
  Typography,
  MenuItem,
  Box,
  Avatar,
  Button,
  Select
} from "@mui/material";
import { FormEvent } from "react";

// components
import BlankCard from "../../shared/BlankCard";
import CustomTextField from "../../forms/theme-elements/CustomTextField";
import CustomFormLabel from "../../forms/theme-elements/CustomFormLabel";
import CustomSelect from "../../forms/theme-elements/CustomSelect";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { MobileDateTimePicker } from "@mui/x-date-pickers/MobileDateTimePicker";
import { CreateUser, GetUser } from "api_service/user";
import { getProTypes } from "api_service/const";

// images
import { Stack } from "@mui/system";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { useRouter } from "next/router";

// locations
const locations = [
  {
    value: "us",
    label: "United States",
  },
  {
    value: "uk",
    label: "United Kingdom",
  },
  {
    value: "india",
    label: "India",
  },
  {
    value: "russia",
    label: "Russia",
  },
];

const genders = [
  {
    value: 0,
    label: "Male",
  },
  {
    value: 1,
    label: "Female",
  },
];

// const aimags = [
//   {
//     value: 0,
//     label: 'Tuv aimag',
//   },
//   {
//     value: 1,
//     label: 'Hovd aimag',
//   },
// ];

const status_types = [
  {
    value: 0,
    label: "Deactive",
  },
  {
    value: 1,
    label: "Active",
  },
];

// currency
const currencies = [
  {
    value: "us",
    label: "US Dollar ($)",
  },
  {
    value: "uk",
    label: "United Kingdom (Pound)",
  },
  {
    value: "india",
    label: "India (INR)",
  },
  {
    value: "russia",
    label: "Russia (Ruble)",
  },
];

const AccountTab = () => {
  let router = useRouter();
  let queryClient = useQueryClient();
  let {userid} = router.query;

  let userInfo = useQuery({
    queryKey: ['userinfo', {
      userid
    }],
    queryFn: GetUser
  })
  
  const { control, handleSubmit, reset, getValues } = useForm();

  useEffect(() => {
    if(userInfo.isLoading){
      reset({})
    }else{
      console.log(userInfo.data);
      reset(userInfo.data ? userInfo.data[0] : {})
    }
  }, [userInfo.isLoading]);

  let aimags_query = useQuery({
    queryKey: [
      "aimags",
      {
        optype: 2,
      },
    ],
    queryFn: getProTypes,
    refetchOnWindowFocus: false,
  });

  let op_types = useQuery({
    queryKey: [
      "op_types",
      {
        optype: 1,
      },
    ],
    queryFn: getProTypes,
    refetchOnWindowFocus: false,
  });

  let mutation = useMutation({
    mutationFn: async (createUser) => CreateUser(createUser),
    onSuccess: () => {
      queryClient.invalidateQueries(['userinfo']);
      router.push('/user');
    }
  })

  const onSubmit: SubmitHandler<any> = (data: any) => {
    mutation.mutate(data);
  };

  return (
    <Grid container spacing={3}>
      {/* Change Profile */}
      <Grid item xs={12} lg={6}>
        <BlankCard>
          <CardContent>
            <Typography variant="h5" mb={1}>
              Change Profile
            </Typography>
            <Typography color="textSecondary" mb={3}>
              Change your profile picture from here
            </Typography>
            <Box textAlign="center" display="flex" justifyContent="center">
              <Box>
                <Avatar
                  src={"/images/profile/user-1.jpg"}
                  alt={"user1"}
                  sx={{ width: 120, height: 120, margin: "0 auto" }}
                />
                <Stack
                  direction="row"
                  justifyContent="center"
                  spacing={2}
                  my={3}
                >
                  <Button variant="contained" color="primary" component="label">
                    Upload
                    <input hidden accept="image/*" multiple type="file" />
                  </Button>
                  <Button variant="outlined" color="error">
                    Reset
                  </Button>
                </Stack>
                <Typography variant="subtitle1" color="textSecondary" mb={4}>
                  Allowed JPG, GIF or PNG. Max size of 800K
                </Typography>
              </Box>
            </Box>
          </CardContent>
        </BlankCard>
      </Grid>
      {/*  Change Password */}
      <Grid item xs={12} lg={6}>
        <BlankCard>
          <CardContent>
            <Typography variant="h5" mb={1}>
              Change Password
            </Typography>
            <Typography color="textSecondary" mb={3}>
              To change your password please confirm here
            </Typography>
            <form>
              <CustomFormLabel
                sx={{
                  mt: 0,
                }}
                htmlFor="text-cpwd"
              >
                Current Password
              </CustomFormLabel>
              <CustomTextField
                id="text-cpwd"
                value="MathewAnderson"
                variant="outlined"
                fullWidth
                type="password"
              />
              {/* 2 */}
              <CustomFormLabel htmlFor="text-npwd">
                New Password
              </CustomFormLabel>
              <CustomTextField
                id="text-npwd"
                value="MathewAnderson"
                variant="outlined"
                fullWidth
                type="password"
              />
              {/* 3 */}
              <CustomFormLabel htmlFor="text-conpwd">
                Confirm Password
              </CustomFormLabel>
              <CustomTextField
                id="text-conpwd"
                value="MathewAnderson"
                variant="outlined"
                fullWidth
                type="password"
              />
            </form>
          </CardContent>
        </BlankCard>
      </Grid>
      {/* Edit Details */}
      <Grid item xs={12}>
        <BlankCard>
          <CardContent>
            <Typography variant="h5" mb={1}>
              Personal Details
            </Typography>
            <Typography color="textSecondary" mb={3}>
              To change your personal detail , edit and save from here
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)} id="profile">
              <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-name"
                  >
                    Firstname
                  </CustomFormLabel>
                  <Controller
                    name="firstname"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-name"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 2 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-store-name"
                  >
                    Lastname
                  </CustomFormLabel>
                  <Controller
                    name="lastname"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-name"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 2 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-store-name"
                  >
                    Register number
                  </CustomFormLabel>
                  <Controller
                    name="reg_number"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-name"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 3 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-location"
                  >
                    Gender
                  </CustomFormLabel>
                  <Controller
                    name="gender"
                    control={control}
                    defaultValue={getValues('gender') || ''}
                    render={({ field }) => (
                      <Select fullWidth id="text-location" variant="outlined" {...field}>
                        {genders.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </Select>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 3 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-location"
                  >
                    Aimag
                  </CustomFormLabel>
                  <Controller
                    name="aimag"
                    control={control}
                    defaultValue={getValues('aimag') || ''}
                    render={({ field }) => (
                      <CustomSelect fullWidth id="text-location" variant="outlined" {...field}>
                        {aimags_query?.data?.map((option: any, index: number) => {
                          return (
                            <MenuItem key={index} value={option.AimagID}>
                              {option.AimagName}
                            </MenuItem>
                          );
                        })}
                      </CustomSelect>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 5 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-email"
                  >
                    Email
                  </CustomFormLabel>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-email"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 6 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-phone"
                  >
                    Phone
                  </CustomFormLabel>
                  <Controller
                    name="phone"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-phone"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 5 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-email"
                  >
                    AppID
                  </CustomFormLabel>
                  <Controller
                    name="app_id"
                    control={control}
                    render={({ field }) => (
                      <CustomSelect fullWidth id="text-location" variant="outlined" {...field}>
                        {op_types?.data?.map((option: any, index: number) => {
                          return (
                            <MenuItem key={index} value={option.AppID}>
                              {option.AppName}
                            </MenuItem>
                          );
                        })}
                      </CustomSelect>
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 5 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-email"
                  >
                    avatar
                  </CustomFormLabel>
                  <Controller
                    name="avatar_url"
                    control={control}
                    render={({ field }) => (
                      <CustomTextField
                        id="text-email"
                        variant="outlined"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* 3 */}
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-location"
                  >
                    Status
                  </CustomFormLabel>
                  <Controller
                    name="status"
                    control={control}
                    defaultValue={getValues('statu') || ''}
                    render={({ field }) => (
                      <CustomSelect fullWidth id="text-location" variant="outlined" {...field}>
                        {status_types.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </CustomSelect>
                    )}
                  />
                </Grid>
                <Grid item xs={12} lg={6} sm={6}>
                  <CustomFormLabel
                    sx={{
                      mt: 0,
                    }}
                    htmlFor="text-location"
                  >
                    Birthday
                  </CustomFormLabel>
                  {/* <ChildCard title="Basic"> */}
                  <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Controller
                      name="dateofbirth"
                      control={control}
                      render={({ field }) => (
                        <MobileDateTimePicker
                          renderInput={(inputProps) => (
                            <CustomTextField
                              fullWidth
                              variant="outlined"
                              inputProps={{ "aria-label": "basic date picker" }}
                              {...inputProps}
                            />
                          )}
                          {...field}
                        />
                      )}
                    />
                  </LocalizationProvider>
                  {/* </ChildCard> */}
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </BlankCard>
        <Stack
          direction="row"
          spacing={2}
          sx={{ justifyContent: "end" }}
          mt={3}
        >
          <Button
            form="profile"
            size="large"
            variant="contained"
            color="primary"
            type="submit"
          >
            Save
          </Button>
          <Button size="large" variant="text" color="error">
            Cancel
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};

export default AccountTab;
